Example `.env`:

```
PORT=3003
CMS_URL=
CMS_USER=
CMS_PASSWORD=
REFRESH_TIMER=300000
DB_CTRL_URL=
```

&#39; => '
&quot; => "
&#8211; => -
&nbsp; => <space>

## tasks

* search tasks:

  * alt image text
  * update algolia

* other:
  * handle captions
  * cloudinary node lib
  * push to mlab
