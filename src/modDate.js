module.exports = (wpDate, imtDate) => {
  // uses valid imt_date, otherwise uses wordpress date
  let date = 0
  if (imtDate !== undefined && imtDate.length > 0) {
    // imt_date format is 20180302
    // js dates are 0-based (Jan = 0, Feb = 1, etc.)
    date = new Date(
      imtDate.substring(0, 4),
      imtDate.substring(4, 6) - 1,
      imtDate.substring(6, 8)
    ).getTime()
  } else date = new Date(wpDate).getTime()
  return date
}
