const Turndown = require('turndown')
const modDate = require('./modDate')
const modAuthors = require('./modAuthors')
const modContent = require('./modContent')

const turndown = new Turndown({ linkStyle: 'referenced' })

module.exports = async (wp, p, cats, wpTags) => {
  const date = modDate(p.date, p.acf.imt_date)
  const authors = await modAuthors(p.id, p.acf.imt_author)

  /* srcset example
  <img
    class=\"size-full wp-image-7812\"
    src=\"https://wp.socialistrevolution.org/wp-content/uploads/2017/12/weinsteinclinton.jpg\"
    alt=\"\"
    width=\"3000\"
    height=\"1996\"
    srcset=\"https://wp.socialistrevolution.org/wp-content/uploads/2017/12/weinsteinclinton.jpg 3000w, https://wp.socialistrevolution.org/wp-content/uploads/2017/12/weinsteinclinton-300x200.jpg 300w, https://wp.socialistrevolution.org/wp-content/uploads/2017/12/weinsteinclinton-768x511.jpg 768w, https://wp.socialistrevolution.org/wp-content/uploads/2017/12/weinsteinclinton-1024x681.jpg 1024w\"
    sizes=\"(max-width: 767px) 89vw, (max-width: 1000px) 54vw, (max-width: 1071px) 543px, 580px\"
  />
  */

  // excerpt
  let excerpt =
    p.acf.imt_excerpt !== undefined && p.acf.imt_excerpt.length > 0
      ? p.acf.imt_excerpt
      : turndown.turndown(p.excerpt.rendered)
  // remove excerpt images and alt text
  const mdImgReg = /!\[(.*?)]\((.*?)\)(.*)/g // matches ![alt text](https://link/img.jpg) and text after in groups
  const match = mdImgReg.exec(excerpt)
  if (Array.isArray(match)) excerpt = match[3]

  let result = {
    id: p.id,
    isSticky: p.sticky,
    slug: p.slug,
    status: p.status,
    categories: [],
    title: turndown.turndown(p.title.rendered),
    date,
    modified: new Date(p.modified).getTime(),
    authors,
    excerpt,
    media: '',
    content: '',
    tags: [],
    order: 99,
  }

  // categories
  p.categories.forEach(catId => {
    const cat = cats.find(d => d.id == catId)
    if (cat !== undefined) {
      result.categories.push(cat)
      if (
        cat.parent !== 0 &&
        result.categories.findIndex(d => d.id === cat.parent) == -1
      ) {
        // has a parent and parent hasn't already been added
        const parent = cats.find(d => d.id == cat.parent)
        if (parent !== undefined) {
          result.categories.push(parent)
        } else console.log(`> parent of ${cat.id}, '${cat.name}', not found`)
      }
    }
  })

  // content
  result.content = await modContent(p.content.rendered, wp)

  // featured media
  let mediaUrl = '/static/imt-wil-logo.jpg'
  if (p.featured_media > 0) {
    const m = await wp.media().id(p.featured_media)
    mediaUrl = m.source_url
  } else {
    // featured_media not set, pull first img from post content
    let newContent = []
    let mediaFound = false
    result.content.forEach(d => {
      if (d.key === 'img' && !mediaFound) {
        mediaUrl = d.val
        mediaFound = true
      } else {
        newContent.push(d)
      }
    })
    result.content = newContent
  }
  result.media = mediaUrl

  // tags
  let promises = p.tags.map(async id => {
    let found = false
    wpTags.forEach(wpTag => {
      if (id === wpTag.id) {
        result.tags.push(wpTag.name)
        found = true
      }
    })
    if (!found) {
      //console.debug(`> ${id} not found, retrieving from wordpress`)
      let wpTag = await wp.tags().id(id)
      wpTags.push(wpTag)
      result.tags.push(wpTag.name)
    }
  })
  await Promise.all(promises)

  return result
}
